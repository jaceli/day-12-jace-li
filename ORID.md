# ORID



## Objective

- This morning I mainly learned React Router, how to configure routes in React projects, and how to jump pages.
- At the same time, I also learned how to use axios to write front-end interfaces. And how to use the call API, while doing exercises about the call API.
- Finally, I also learned Ant Design, a common component library used in react, and was able to make some beautification of my own pages.



## Reflective

- fruitful and challenging.


## Interpretative

- I am not familiar with the use of Redux and Router, as well as the writing of front-end interfaces, custom hooks.

## Decision

- I will continue to study hard in the next course and finish the homework on time.