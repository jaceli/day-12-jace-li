import request from "./request";

// get all todos
export const loadTodos = () => {
    return request.get("/todos")
}

// update a todo status
export const toggleTodo = (id, data) => {
    return request.put(`/todos/${id}`, {
        done: data.done,
    })
}

// add a new todo
export const createTodo = (data) => {
    return request.post('/todos', {
        text: data.text
    })
}

// delete a todo
export const deleteTodo = (id) => {
    return request.delete(`/todos/${id}`)
}

// update a todo text
export const updateTodoText = (id,data) => {
    return request.put(`/todos/${id}`, {
        text: data.text,
    })
}

// get todo by id
export const loadTodoById = (id) => {
    return request.get(`/todos/${id}`)
}