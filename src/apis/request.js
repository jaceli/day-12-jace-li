import { message } from "antd";
import axios from "axios";

const request = axios.create({
    baseURL: "http://localhost:8081/",
})

request.interceptors.response.use(
    (response) => response,
    (error) => {
        console.log(error.response);
        // message.info(error.response.data?.msg)
        return Promise.reject(error)
    }
) 

export default request