import { createBrowserRouter, Link } from "react-router-dom";
import { TodoList } from "./components/TodoList";
import AboutPage from "./pages/AboutPage";
import DoneList from "./pages/todo/DoneList";
import Layout from "./components/Layout";
import TodoDetail from "./pages/todo/TodoDetail"
import NotFoundPage from "./pages/NotFoundPage"

export const router = createBrowserRouter([
  {
    path: "/",
    element: (<Layout />),
    children: [
      {
        index: true,  // 等价于 path: "/"
        element: (<TodoList />),
      },
      {
        path: "about",
        element: <AboutPage />
      },
      {
        path: "done",
        element: <DoneList />
      },
      {
        path: "todo/:id",
        element: <TodoDetail />
      },
      {
        path: '*',
        element: <NotFoundPage />
      }
    ]
  },

]);
