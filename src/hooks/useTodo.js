
import { loadTodos } from './../apis/todo';
import { initTodos } from '../components/todoSlice';
import { useDispatch } from 'react-redux';
import { useRef } from 'react';


export const useTodo = () => {
    const dispatch = useDispatch()
    

    const reloadTodos = () => {
        loadTodos().then((response) => {
            dispatch(initTodos(response.data))
        })
    }

    return useRef({reloadTodos}).current
}