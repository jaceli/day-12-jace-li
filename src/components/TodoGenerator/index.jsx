import { useState } from 'react'
import './index.css'
import { createTodo } from '../../apis/todo'
import { useTodo } from '../../hooks/useTodo'
import { Button, Input } from 'antd'

export const TodoGenerator = () => {
    const [inputTodo, setInputTodo] = useState('')
    const { reloadTodos } = useTodo()

    const addTodo = async () => {
        if (inputTodo.trim() === '') {
            alert("请输入内容")
            return
        }
        const dic = { text: inputTodo }
        await createTodo(dic)
        reloadTodos()
        // dispatch(addNewTodo(dic))
        setInputTodo('');
    }

    const handleKeyUp = (event) => {
        if (event.keyCode === 13) {
            addTodo()
        }
    }
    return (
        <div className='todo-header'>
            <Input type='text'
                value={inputTodo}
                placeholder='input a new todo here...'
                onChange={(event) => setInputTodo(event.target.value)}
                onKeyUp={handleKeyUp}
            ></Input>
            <Button onClick={addTodo} type='primary'>Add</Button>
        </div>
    )
}
