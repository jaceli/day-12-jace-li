import { Outlet, NavLink } from "react-router-dom";
import { useState } from "react";
import { Menu } from "antd";

const Layout = () => {
  const items = [
    {
      label: (
        <NavLink to='/'>HOME</NavLink>
      ),
      key: 'home',
    },
    {
      label: (
        <NavLink to='/about'>About</NavLink>
      ),
      key: 'about',
    },
    {
      label: (
        <NavLink to='/done'>Done List</NavLink>
      ),
      key: 'donelist',
    }
  ]

  const [current, setCurrent] = useState('home');
  const onClick = (e) => {
    setCurrent(e.key);
  };
  return (
    <div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Menu style={{ display: "flex", width: "300px" }} onClick={onClick} selectedKeys={[current]} items={items} />
      </div>
      <Outlet />
    </div>
  );
}

export default Layout;