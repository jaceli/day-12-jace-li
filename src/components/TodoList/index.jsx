import { useSelector } from 'react-redux';
import { TodoGenerator } from '../TodoGenerator/';
import { TodoGroup } from '../TodoGroup/';
import { useEffect } from 'react';
import { useTodo } from '../../hooks/useTodo';

export const TodoList = () => {
    const todoList = useSelector(state => state.todo.todoList)
    const {reloadTodos} = useTodo()

    useEffect(() => {
        reloadTodos()
    }, [reloadTodos])

    return (
        <div className='todoList'>
            <TodoGroup todoList={todoList} isAlter={true}/>
            <TodoGenerator />
        </div>
    )
}
