import { TodoItem } from "../TodoItem";


export const TodoGroup = (props) => {
    const todoList = props.todoList

    const todoItems = todoList.map((item) => {
        return <TodoItem todo={item} key={item.id} isAlter={props.isAlter } />
    });
    return (
        <div className="todoGroup">
            <h3>Todo List</h3>
            {todoItems}
        </div>
    )
}
