import './index.css'
import { useNavigate } from 'react-router-dom'
import { deleteTodo, toggleTodo, updateTodoText } from '../../apis/todo'
import { useTodo } from '../../hooks/useTodo'
import { Button, Modal, Input, message } from 'antd';
import { CloseCircleOutlined, ExclamationCircleFilled, EditOutlined } from '@ant-design/icons'
import { useState } from 'react'




export const TodoItem = (props) => {
    const { todo } = props
    const navigate = useNavigate()
    const { reloadTodos } = useTodo()
    const { confirm } = Modal

    const [inputValue, setInputValue] = useState('')

    const [isEditing, setIsEditing] = useState(false)

    const deleteOneTodo = (event) => {
        event.stopPropagation()
        confirm({
            title: 'Are you sure delete this task?',
            icon: <ExclamationCircleFilled />,
            content: todo.text,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            async onOk() {
                const id = props.todo.id
                await deleteTodo(id)
                reloadTodos()
            },
        });
    }

    const editText = (event) => {
        event.stopPropagation()
        setInputValue(todo.text)
        setIsEditing(true)
    }

    const handleUpdate = async (e) => {
        if (inputValue.trim() === '') {
            alert('请输入内容');
            setIsEditing(false)
            return
        }
        const data = { "text": inputValue.trim(), "description": "this is a description" }
        await updateTodoText(todo.id, data)
        reloadTodos()
        setIsEditing(false)
    };

    const handleChange = (e) => {
        setInputValue(e.target.value);
    };

    const toggle = async () => {
        if (props.isAlter) {
            // api请求
            const data = {"done": !todo.done}
            await toggleTodo(todo.id, data)
            reloadTodos()
        } else {
            navigate(`/todo/${todo.id}`)
        }
    }


    return (
        <>
        <ul onClick={toggle} style={{display: 'flex', justifyContent: 'center', padding: '0 10px'}}>
            <li>
                <span style={{ textDecoration: todo.done ? "line-through" : "none", wordWrap: "break-word" }}>{todo.text}</span>
                <Button className='hidden' onClick={editText} icon={<EditOutlined />} disabled={!props.isAlter}></Button>
                <Button className='hidden .dtn' onClick={deleteOneTodo} icon={<CloseCircleOutlined />} disabled={!props.isAlter}></Button>
            </li>
        </ul>
        <Modal
            title="Edit Text"
            open={isEditing}
            onOk={handleUpdate}
            onCancel={(e) => {
                setIsEditing(false)
            }}>
            <Input value={inputValue} onChange={handleChange} allowClear></Input>
        </Modal>
        </>
    )
}