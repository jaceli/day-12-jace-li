import { useEffect, useState } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import { loadTodoById } from "../../apis/todo";


const TodoDetail = () => {
    const params = useParams()  // /id
    const { id } = params
    const navigete = useNavigate()

    //const [searchParams] = useSearchParams() // ?id=
    const [todo, setTodo] = useState()

    const getTodoById = async () => {
        try {
            const response = await loadTodoById(id)
            setTodo(response.data)
        } catch (error) {
            navigete("/404")
        }
    }


    useEffect(() => {
        getTodoById()
    }, [])


    return (
        <div>
            <h1>Detail</h1>
            {todo && <p>{todo.text}</p>}
        </div>
    );
}

export default TodoDetail;