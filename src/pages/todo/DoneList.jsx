import { useSelector } from 'react-redux';
import { TodoGroup } from '../../components/TodoGroup';
import { useTodo } from '../../hooks/useTodo';
import { useEffect } from 'react';

const DoneList = () => {


  const todoList = useSelector(state => state.todo.todoList).filter(todo => {
    return todo.done
  })

  const { reloadTodos } = useTodo()
  
  useEffect(() => {
    reloadTodos()
  }, [reloadTodos])


  return ( 
    <div>
      <h1>Done List</h1>
      <TodoGroup todoList={todoList} isAlter={false}/>
    </div>
   );
}
 
export default DoneList;